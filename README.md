# building an aydin (gpu enabled) from conda for singularity and docker image for CI

Tru <tru@pasteur.fr>

## Why ?
- just because it is fun
- build docker image from dockerhub registry and push to registry.pasteur.fr with proper tags
- build docker image, push to registry.pasteur.fr and re-use that docker image to create a singularity container as artefact
- 
## Caveat
- playground, use at your own risk!
- `:main` tagged docker image
- `:latest` tagged singularity image

<!--
## Usage
- Docker [![Docker build](https://github.com/truatpasteurdotfr/singularity-docker-aydin-gpu/actions/workflows/docker-publish.yml/badge.svg)](https://github.com/truatpasteurdotfr/singularity-docker-aydin-gpu/actions/workflows/docker-publish.yml)
```
docker pull ghcr.io/truatpasteurdotfr/singularity-docker-aydin-gpu:main
```

- Singularity [![Singularity build](https://github.com/truatpasteurdotfr/singularity-docker-aydin-gpu/actions/workflows/singularity-publish.yml/badge.svg)](https://github.com/truatpasteurdotfr/singularity-docker-aydin-gpu/actions/workflows/singularity-publish.yml)
```
singularity run oras://ghcr.io/truatpasteurdotfr/singularity-docker-aydin-gpu:latest
```
--/>
